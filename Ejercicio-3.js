db.getCollection("paciente").insertMany([
                                                {
                                                    "Nombres":"Pablo Fernandez",
                                                    "Edad": 22,
                                                    "Genero":"Masculino",
                                                    "Estado": "Ingresado",
                                                    "EstadoCivil": "Soltero",
                                                    "Direccion":"Av. San Martin #123"
                                                },
                                                {
                                                    "Nombres":"Erlinda Martinez",
                                                    "Edad":20,
                                                    "Genero":"Femenino",
                                                    "Estado": "Ingresado",
                                                    "EstadoCivil": "Soltera",
                                                    "Direccion":"Av. 16 de Julio #512"
                                                },
                                                {
                                                    "Nombres":"Ramiro Valdez",
                                                    "Edad":18,
                                                    "Genero":"Masculino",
                                                    "Estado": "Recuperado",
                                                    "EstadoCivil": "Casado",
                                                    "Diereccion":"Av. Los Olmos # 16"
                                                },
                                                {
                                                    "Nombres":"Gabriel Rojas",
                                                    "Edad": 25,
                                                    "Genero":"Masculino",
                                                    "Estado": "Recuperado",
                                                    "EstadoCivil": "Casado",
                                                    "Direccion":"Av. Heroinas #623"
                                                },
                                                {
                                                    "Nombres":"Luis Mancilla",
                                                    "Edad":24,
                                                    "Genero":"Masculino",
                                                    "Estado": "Ingresado",
                                                    "EstadoCivil": "Soltero",
                                                    "Direccion":"Av. Petrolera #497"
                                                }
                                            ])
                                                
                                                
                                         
db.getCollection("paciente").createIndex({"Nombres":1})
db.getCollection("paciente").createIndex({"Estado":1})

db.getCollection("paciente").find({})

db.getCollection("paciente").find({"Estado": "Ingresado"})

db.getCollection("paciente").find({})